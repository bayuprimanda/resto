
package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.ReturnHdrModel;

public interface ReturnHdrDao {

	public List<ReturnHdrModel> get() throws Exception;

	public void insert(ReturnHdrModel model) throws Exception;

	public ReturnHdrModel getById(int id) throws Exception;

	public void update(ReturnHdrModel model) throws Exception;

	public void delete(ReturnHdrModel model) throws Exception;
}
