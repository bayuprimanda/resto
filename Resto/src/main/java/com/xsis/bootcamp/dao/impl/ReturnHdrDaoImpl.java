package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.ReturnHdrDao;
import com.xsis.bootcamp.model.ReturnHdrModel;


@Repository
public class ReturnHdrDaoImpl implements ReturnHdrDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ReturnHdrModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<ReturnHdrModel> result = session.createQuery("from ReturnHdrModel").list();
		return result;
	}

	@Override
	public void insert(ReturnHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ReturnHdrModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		ReturnHdrModel result = session.get(ReturnHdrModel.class, id);
		return result;
	}

	@Override
	public void update(ReturnHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ReturnHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
		
	}

}
