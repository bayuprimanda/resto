package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.ReturnDtlDao;
import com.xsis.bootcamp.model.ReturnDtlModel;

@Repository
public class ReturnDtlDaoImpl implements ReturnDtlDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ReturnDtlModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<ReturnDtlModel> result = session.createQuery("from ReturnDtlModel").list();
		return result;
	}

	@Override
	public void insert(ReturnDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ReturnDtlModel getById(String id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		ReturnDtlModel result = session.get(ReturnDtlModel.class, id);
		return result;
	}

	@Override
	public void update(ReturnDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ReturnDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
