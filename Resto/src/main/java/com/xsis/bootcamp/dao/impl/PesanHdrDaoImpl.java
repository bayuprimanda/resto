package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PesanHdrDao;
import com.xsis.bootcamp.model.PesanHdrModel;

@Repository
public class PesanHdrDaoImpl implements PesanHdrDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<PesanHdrModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PesanHdrModel> result = session.createQuery("from PesanHdrModel").list();
		return result;
	}

	@Override
	public void insert(PesanHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public PesanHdrModel getById(String id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PesanHdrModel result = session.get(PesanHdrModel.class, id);
		return result;
	}

	@Override
	public void update(PesanHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
	}

	@Override
	public void delete(PesanHdrModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
		
	}
	
	

}
