package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PesanHdrModel;

public interface PesanHdrDao {

	public List<PesanHdrModel> get() throws Exception;

	public void insert(PesanHdrModel model) throws Exception;

	public PesanHdrModel getById(String id) throws Exception;

	public void update(PesanHdrModel model) throws Exception;

	public void delete(PesanHdrModel model) throws Exception;
}
