package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PesanDtlModel;

public interface PesanDtlDao {
	
	public List<PesanDtlModel> get() throws Exception;

	public void insert(PesanDtlModel model) throws Exception;

	public PesanDtlModel getById(int id) throws Exception;

	public void update(PesanDtlModel model) throws Exception;

	public void delete(PesanDtlModel model) throws Exception;
}
