package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PesanDtlDao;
import com.xsis.bootcamp.model.PesanDtlModel;

@Repository
public class PesanDtlDaoImpl implements PesanDtlDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<PesanDtlModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PesanDtlModel> result = session.createQuery("from PesanDtlModel").list();
		return result;
	}

	@Override
	public void insert(PesanDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
	}

	@Override
	public PesanDtlModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PesanDtlModel result = session.get(PesanDtlModel.class, id);
		return result;
	}

	@Override
	public void update(PesanDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
	}

	@Override
	public void delete(PesanDtlModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
		
	}

}
