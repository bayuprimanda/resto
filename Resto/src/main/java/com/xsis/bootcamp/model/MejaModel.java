package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name = "MEJA")
public class MejaModel {
	
	private Integer id;
	private String nomor;
	private String tipe;
	private String status;
	private String ket;
	
	
	@Id
	@GeneratedValue
	@Column(name="ID_MEJA")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(unique = true, name="NO_MEJA")
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	
	@Column(name="TIPE_MEJA")
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	
	@Column(name="STS_MEJA")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="KET_MEJA")
	@NotBlank
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}

}
