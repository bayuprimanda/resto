package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PESAN_DTL")
public class PesanDtlModel {
	
	private Integer id;
	
	//FK
	private String id_ps;
	private Integer id_mn;
	
	//DTL
	private String nm_mn;
	private Integer hrg_jl;
	private Integer qty;
	private Integer subtot;
	
	
	//FK
	private MenuModel menu;
	private PesanHdrModel pesan;
	
	
	//FK
	@Id
	@GeneratedValue
	@Column(name="ID_DTL")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ID_HDR")
	public String getId_ps() {
		return id_ps;
	}
	public void setId_ps(String id_ps) {
		this.id_ps = id_ps;
	}
	
	@Column(name="ID_MENU")
	public Integer getId_mn() {
		return id_mn;
	}
	public void setId_mn(Integer id_mn) {
		this.id_mn = id_mn;
	}
	
	
	//DTL
	@Column(name="NAMA_MENU")
	public String getNm_mn() {
		return nm_mn;
	}
	public void setNm_mn(String nm_mn) {
		this.nm_mn = nm_mn;
	}
	
	@Column(name="HARGA_JUAL")
	public Integer getHrg_jl() {
		return hrg_jl;
	}
	public void setHrg_jl(Integer hrg_jl) {
		this.hrg_jl = hrg_jl;
	}
	
	@Column(name="QTY")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name="SUBTOT")
	public Integer getSubtot() {
		return subtot;
	}
	public void setSubtot(Integer subtot) {
		this.subtot = subtot;
	}
	
	
	//FK
	@ManyToOne
	@JoinColumn(name="ID_MENU", nullable=false, updatable=false, insertable=false)
	public MenuModel getMenu() {
		return menu;
	}
	public void setMenu(MenuModel menu) {
		this.menu = menu;
	}
	
	
	@ManyToOne
	@JoinColumn(name="ID_HDR", nullable=false, updatable=false, insertable=false)
	public PesanHdrModel getPesan() {
		return pesan;
	}
	public void setPesan(PesanHdrModel pesan) {
		this.pesan = pesan;
	}
}
