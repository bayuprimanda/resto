package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DTL_RSV")
public class DetailReservasiModel {
	
	private Integer id;
	private Integer id_rs;
	private Integer id_mj;
	private String nomor;
	private String tipe;
	private String ket;
	private MejaModel meja;
	private ReservasiModel reservasi;
	
	
	@Id
	@GeneratedValue
	@Column(name="ID_DTL_RESV")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ID_RESV")
	public Integer getId_rs() {
		return id_rs;
	}
	public void setId_rs(Integer id_rs) {
		this.id_rs = id_rs;
	}
	
	@Column(name="ID_MEJA")
	public Integer getId_mj() {
		return id_mj;
	}
	public void setId_mj(Integer id_mj) {
		this.id_mj = id_mj;
	}
	
	@Column(name="NOMOR")
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	
	@Column(name="TIPE")
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	
	@Column(name="KET")
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	

	@ManyToOne
	@JoinColumn(name="ID_MEJA", nullable=false, updatable=false, insertable=false)
	public MejaModel getMeja() {
		return meja;
	}
	public void setMeja(MejaModel meja) {
		this.meja = meja;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_RESV", nullable=false, updatable=false, insertable=false)
	public ReservasiModel getReservasi() {
		return reservasi;
	}
	public void setReservasi(ReservasiModel reservasi) {
		this.reservasi = reservasi;
	}

}
