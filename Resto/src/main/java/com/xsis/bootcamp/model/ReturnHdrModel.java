package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="RETURN_HDR")
public class ReturnHdrModel {

	
		private Integer id;
		private String tgl_rtr;
		private String ket;
		
		//FK
		private Integer id_sp;
		private SupplierModel sup;
		
		private List<ReturnDtlModel> detail;
		
		@Id
		@GeneratedValue
		@Column(name="ID_HDR")
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name="TGL_RETUR")
		public String getTgl_rtr() {
			return tgl_rtr;
		}
		public void setTgl_rtr(String tgl_rtr) {
			this.tgl_rtr = tgl_rtr;
		}
		
		@Column(name="KETERANGAN")
		public String getKet() {
			return ket;
		}
		public void setKet(String ket) {
			this.ket = ket;
		}
		
		@Column(name="ID_SUP")
		public Integer getId_sp() {
			return id_sp;
		}
		public void setId_sp(Integer id_sp) {
			this.id_sp = id_sp;
		}
		
		@ManyToOne
		@JoinColumn(name="ID_SUP", nullable=false, insertable=false, updatable=false)
		public SupplierModel getSup() {
			return sup;
		}
		public void setSup(SupplierModel sup) {
			this.sup = sup;
		}
		
		@OneToMany(fetch=FetchType.EAGER, mappedBy="retur")
		public List<ReturnDtlModel> getDetail() {
			return detail;
		}
		public void setDetail(List<ReturnDtlModel> detail) {
			this.detail = detail;
		}
		
}
