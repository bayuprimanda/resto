package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="PESAN_HDR")
public class PesanHdrModel {
	
	//#1st HEADER
	private String id;
	private String date;
	private String nama;
	private Integer id_mj;
	
	
	//#2nd HEADER
	private Integer total;
	private String tp_bayar;
	private Integer bayar;
	private Integer kembalian;
	
	//FK MejaModel
	
	private MejaModel mj;
	private List<PesanDtlModel> detail;
	
	
	
	//1st GETTER SETTER HEADER
	@Id
	@Column(name="ID_HDR", unique=true)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name="TANGGAL")
	public String getDate() {
		return date;
	}
	public void setDate(String tgl_pembelian) {
		this.date = tgl_pembelian;
	}
	
	@Column(name="NAMA")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="ID_MEJA")
	public Integer getId_mj() {
		return id_mj;
	}
	public void setId_mj(Integer id_mj) {
		this.id_mj = id_mj;
	}
	
	
	//2# GETTER SETTER HEADER
	@Column(name="TOTAL_BAYAR")
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	@Column(name="TIPE_BAYAR")
	public String getTp_bayar() {
		return tp_bayar;
	}
	public void setTp_bayar(String tp_bayar) {
		this.tp_bayar = tp_bayar;
	}
	
	@Column(name="JML_BAYAR")
	public Integer getBayar() {
		return bayar;
	}
	public void setBayar(Integer bayar) {
		this.bayar = bayar;
	}
	
	@Column(name="KEMBALIAN")
	public Integer getKembalian() {
		return kembalian;
	}
	public void setKembalian(Integer kembalian) {
		this.kembalian = kembalian;
	}
	
	
	
	//JOIN COLUMN
	@ManyToOne
	@JoinColumn(name="ID_MEJA", nullable=false, insertable=false, updatable=false)
	public MejaModel getMj() {
		return mj;
	}
	public void setMj(MejaModel mj) {
		this.mj = mj;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="pesan")
	public List<PesanDtlModel> getDetail() {
		return detail;
	}
	public void setDetail(List<PesanDtlModel> detail) {
		this.detail = detail;
	}	
	
	
}
