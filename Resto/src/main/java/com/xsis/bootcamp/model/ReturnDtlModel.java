package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="RETURN_DTL")
public class ReturnDtlModel {
	
		//DTL
		private Integer id;
		private Integer id_rtr;
		private Integer id_bhn;
		private String nama;
		private String Satuan;
		private Integer qty;
		
		//FK
		private BahanModel bahan;
		private ReturnHdrModel retur;
		
		
		@Id
		@GeneratedValue
		@Column(name="ID_DTL")
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name="ID_HDR")
		public Integer getId_rtr() {
			return id_rtr;
		}
		public void setId_rtr(Integer id_rtr) {
			this.id_rtr = id_rtr;
		}
		
		@Column(name="ID_BAHAN")
		public Integer getId_bhn() {
			return id_bhn;
		}
		public void setId_bhn(Integer id_bhn) {
			this.id_bhn = id_bhn;
		}
		
		@Column(name="NAMA_BAHAN")
		public String getNama() {
			return nama;
		}
		public void setNama(String nama) {
			this.nama = nama;
		}
		
		@Column(name="SATUAN")
		public String getSatuan() {
			return Satuan;
		}
		public void setSatuan(String satuan) {
			Satuan = satuan;
		}
		
		@Column(name="QTY")
		public Integer getQty() {
			return qty;
		}
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		
		
		@ManyToOne
		@JoinColumn(name="ID_BAHAN", nullable=false, insertable=false, updatable=false)
		public BahanModel getBahan() {
			return bahan;
		}
		public void setBahan(BahanModel bahan) {
			this.bahan = bahan;
		}
		
		@ManyToOne
		@JoinColumn(name="ID_HDR", nullable=false, insertable=false, updatable=false)
		public ReturnHdrModel getRetur() {
			return retur;
		}
		public void setRetur(ReturnHdrModel retur) {
			this.retur = retur;
		}
		
		
}
