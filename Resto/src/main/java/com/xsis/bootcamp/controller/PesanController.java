package com.xsis.bootcamp.controller;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.bootcamp.model.MejaModel;
import com.xsis.bootcamp.model.MenuModel;
import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.model.PesanHdrModel;
import com.xsis.bootcamp.service.MejaService;
import com.xsis.bootcamp.service.MenuService;
import com.xsis.bootcamp.service.PesanDtlService;
import com.xsis.bootcamp.service.PesanHdrService;


@Controller
public class PesanController {
	private Log log = LogFactory.getLog(getClass());


	@Autowired
	private MenuService menuService;
	
	@Autowired
	private MejaService mejaService;
	
	@Autowired
	private PesanHdrService hdrService;
	
	@Autowired
	private PesanDtlService dtlService;

	List<PesanDtlModel> pdmList = null;
	
	@RequestMapping(value="/listpesan")
	public String ListPesan(Model model){
		
		return "listpesan";
	}
	
	// HEADER //
	
	//METHOD TAMPLIAN HEADER
	//COMBO BOX MEJA
	@RequestMapping(value="/pesan")
	public String index(Model model,HttpServletRequest request) {
		List<MejaModel> meja = null;
		pdmList = new ArrayList<PesanDtlModel>();		
		
		try {
			meja = this.mejaService.get();
		} catch (Exception e) {
			
		}
		List<MejaModel> listTersedia = new ArrayList<MejaModel>();
		for (int i = 0; i < meja.size(); i++) {
			if (meja.get(i).getStatus().equals("Tersedia")) {
				listTersedia.add(meja.get(i));
			}
			
		}
		model.addAttribute("listMeja", listTersedia);
		
		return "pesan";
	}
	
	//METHOD LIST SELURUH TRANSAKSI
	@RequestMapping(value="/pesan/list")
	public String list(Model model) {
		
		List<PesanHdrModel> items = null;
		
		try {
			items = this.hdrService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("listPesan", items);
		
		return "pesan/list";
	}
	
	//METHOD DETAIL DARI TIAP TRANSAKSI
	@RequestMapping(value="/pesan/detail")
	public String detail(Model model, HttpServletRequest request) {
		
		String id = request.getParameter("id");
		PesanHdrModel item = null;
		
		try {
			item = this.hdrService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("item", item);
		
		return "pesan/detail";
	}
	
	
	// DETAIL //
	//METHOD PILIH MENU
	@RequestMapping(value="/pesan/pilih_menu")
	public String pilih_detail(Model model, HttpServletRequest request,@RequestParam(value="itemPilih") String[] itemIdPilih) throws Exception{
		
		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] =itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");
			
			MenuModel im = new MenuModel();
			im = this.menuService.getById(Integer.valueOf(itemIdPilih[i]));
			
			PesanDtlModel jdm = new PesanDtlModel();
			jdm.setId_mn(im.getId_mkn());
			jdm.setMenu(im);
			
			
			pdmList.add(jdm);
		}
		 model.addAttribute("pdmList", pdmList);
		 model.addAttribute("success", "success");
		
		
		return "penjualan_header";
	}
	
	// METHOD POP UP PENAMBAHAN PESANAN
	@RequestMapping(value = "/pesan/add")
	public String add(Model model) {
		List<MenuModel> menus = null;
		String idExcludeList = "";
		try {
			// object items diisi data dari method get
			idExcludeList = this.getIdExcludeList(pdmList);
			menus = this.menuService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		List<MenuModel> listTersedia = new ArrayList<MenuModel>();
		for (int i = 0; i < menus.size(); i++) {
			if (menus.get(i).getStock().equals("Available")) {
				listTersedia.add(menus.get(i));
			}
			
		}
		model.addAttribute("list", listTersedia);
		return "pesan/add";
	}
	
	
	// METHOD PEMBATALAN PESANAN
		@RequestMapping(value = "/pesan/batal_pesan")
		public String batal_pesan(Model model, HttpServletRequest request) {

			int id = Integer.valueOf(request.getParameter("idx"));
			if (pdmList.size() > 0) {
				for (int i = 0; i < pdmList.size(); i++) {
					int idMenu = pdmList.get(i).getId_mn();
					if (id == idMenu) {
						pdmList.remove(i);
					}
				}
			}

			model.addAttribute("pdmList", pdmList);
			model.addAttribute("success", "success");
			return "pesan";
		}

		// METHOD KONDISI PILIH
		public String getIdExcludeList(List<PesanDtlModel> pdmList) {
			String idExcludeList = "";
			String comma = "";
			if (pdmList.size() > 0) {
				for (int i = 0; i < pdmList.size(); i++) {
					if (i > 0) {
						comma = ",";
					} else {
						comma = " ";
					}

					int id = pdmList.get(i).getId_mn();
					idExcludeList = idExcludeList + comma + id;
				}
			}

			return idExcludeList;
		}
		
		// METHOD PENYIMPANAN DATA DARI HEADER & DETAIL KE DALAM DATABASE
		@SuppressWarnings("unused")
		@RequestMapping(value="/pesan/simpan")
		public String save(Model model, HttpServletRequest request) throws Exception {
		
			String result ="";
			
			//Get data dari form header
			String id_pesan = request.getParameter("id");
			String np = request.getParameter("n_psn");
			String id_mj = request.getParameter("id_meja");
			String total_jual = request.getParameter("total_jual");
			String tipe_bayar = request.getParameter("tipe_bayar");
			String bayar = request.getParameter("bayar");
			String kembalian = request.getParameter("kembalian");
			
			String tgl_pembelian = request.getParameter("tgl_psn");
			
			//Set data dari form header ke model header
			PesanHdrModel phm = new PesanHdrModel();
			//set
			phm.setId(id_pesan);
			phm.setNama(np);
			phm.setId_mj(Integer.valueOf(id_mj));
				MejaModel meja = new MejaModel();
				meja = this.mejaService.getById(Integer.valueOf(id_mj));
			phm.setTotal(Integer.valueOf(total_jual));
			phm.setTp_bayar(tipe_bayar);
			phm.setBayar(Integer.valueOf(bayar));
			phm.setKembalian(Integer.valueOf(kembalian));
			phm.setDate(tgl_pembelian);
			try {
				//simpan pembelian detail
				this.hdrService.insert(phm);
				String HeaderID = phm.getId();
				
				String jumlahDetail = request.getParameter("jumlahDetail");
				int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
				
				
				for (int i = 0; i < jumlahDetailInteger; i++) {
					//Set per detil satu satu
					String idMenu = request.getParameter("idMenu_"+i);
					String nm = request.getParameter("namaMenu_"+i);
					String hargajual = request.getParameter("hargajual_"+i);
					String qtyDetail = request.getParameter("qty_"+i);
					String hasilDetail = request.getParameter("hasil_"+i);
					
					MenuModel item = new MenuModel();
					item = this.menuService.getById(Integer.valueOf(idMenu));
					
					
					PesanDtlModel dm = new PesanDtlModel();
					dm.setId_mn(Integer.valueOf(idMenu));
					dm.setMenu(item);
					dm.setId_ps(HeaderID);
					dm.setNm_mn(nm);
					dm.setHrg_jl(Integer.valueOf(hargajual));
					dm.setQty(Integer.valueOf(qtyDetail));
					dm.setSubtot (Integer.valueOf(hasilDetail));
					
					//Simpan per detil satu satu
					this.dtlService.insert(dm);
				}
				
				result="berhasil";
				
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage(), e);
				result ="gagal";
				
			}
			
			model.addAttribute("result",result);
			
			return "pesan/simpan";
			
		}

	}
