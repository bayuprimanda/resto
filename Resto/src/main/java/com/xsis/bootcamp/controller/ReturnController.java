package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.xsis.bootcamp.model.ReturnDtlModel;
import com.xsis.bootcamp.model.ReturnHdrModel;
import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.model.BahanModel;
import com.xsis.bootcamp.service.BahanService;
import com.xsis.bootcamp.service.ReturnDtlService;
import com.xsis.bootcamp.service.ReturnHdrService;
import com.xsis.bootcamp.service.SupplierService;

@Controller
public class ReturnController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SupplierService supService;
	
	@Autowired
	private BahanService bhnService;
	
	@Autowired
	private ReturnHdrService hdrService;
	
	@Autowired
	private ReturnDtlService dtlService;

	List<ReturnDtlModel> rtrList = null;
	
	@RequestMapping(value="/listreturn")
	public String ListReturn(Model model){
		
		return "listreturn";
	}
	
		// HEADER //
		@RequestMapping(value="/return")
		public String index(Model model,HttpServletRequest request) {
			List<SupplierModel> supplier = null;
			rtrList = new ArrayList<ReturnDtlModel>();		
			
			try {
				supplier = this.supService.get();
			} catch (Exception e) {
				
			}
			
			model.addAttribute("listSupplier", supplier);
			
			return "return";
		}
		
		//METHOD LIST SELURUH TRANSAKSI
		@RequestMapping(value="/return/list")
		public String list(Model model) {
			
			List<ReturnHdrModel> items = null;
			
			try {
				items = this.hdrService.get();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			
			model.addAttribute("listReturn", items);
			
			return "return/list";
		}
		
		//METHOD DETAIL DARI TIAP TRANSAKSI
		@RequestMapping(value="/return/detail")
		public String detail(Model model, HttpServletRequest request) {
			
			int id = Integer.valueOf(request.getParameter("id"));
			ReturnHdrModel item = null;
			
			try {
				item = this.hdrService.getById(id);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			
			model.addAttribute("item", item);
			
			return "return/detail";
		}
		
		// DETAIL //
		//Batal Return
		
		// batal meja
		@RequestMapping(value = "/return/batal_return")
		public String batal_return(Model model, HttpServletRequest request) {

			int id = Integer.valueOf(request.getParameter("idx"));
			if (rtrList.size() > 0) {
				for (int i = 0; i < rtrList.size(); i++) {
					int idReturn = rtrList.get(i).getId_bhn();
					if (id == idReturn) {
						rtrList.remove(i);
					}
				}
			}

			model.addAttribute("rtrList", rtrList);
			model.addAttribute("success", "success");
			return "return";
		}
		
		
	// pilih Retur
	@RequestMapping(value = "/return/pilih_return")
	public String pilih_detail(Model model, HttpServletRequest request,
			@RequestParam(value = "itemPilih") String[] itemIdPilih) throws Exception {

		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] = itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");

			BahanModel im = new BahanModel();
			im = this.bhnService.getById(Integer.valueOf(itemIdPilih[i]));

			ReturnDtlModel jdm = new ReturnDtlModel();
			jdm.setId_bhn(im.getId());
			jdm.setBahan(im);
			
			rtrList.add(jdm);
		}
		model.addAttribute("rtrList", rtrList);
		model.addAttribute("success", "success");

		return "return";
	}

	// tambah Retur
	@RequestMapping(value = "/return/add")
	public String add(Model model) {

		List<BahanModel> listRetur = null;
		String idExcludeList = "";
		try {
			idExcludeList = this.getIdExcludeList(rtrList);
			listRetur = this.bhnService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("listRetur", listRetur);
		return "return/add";
	}

	// batal Retur
	@RequestMapping(value = "/return/batal_retur")
	public String batal_Retur(Model model, HttpServletRequest request) {

		int id = Integer.valueOf(request.getParameter("idx"));
		if (rtrList.size() > 0) {
			for (int i = 0; i < rtrList.size(); i++) {
				int idRetur = rtrList.get(i).getId_bhn();
				if (id == idRetur) {
					rtrList.remove(i);
				}
			}
		}

		model.addAttribute("rtrList", rtrList);
		model.addAttribute("success", "success");
		return "return";
	}

	// list ex
	public String getIdExcludeList(List<ReturnDtlModel> rtrList) {
		String idExcludeList = "";
		String comma = "";
		if (rtrList.size() > 0) {
			for (int i = 0; i < rtrList.size(); i++) {
				if (i > 0) {
					comma = ",";
				} else {
					comma = " ";
				}

				int id = rtrList.get(i).getId_bhn();
				idExcludeList = idExcludeList + comma + id;
			}
		}

		return idExcludeList;
	}
	
	@RequestMapping(value="/return/simpan")
	public String cetak(Model model, HttpServletRequest request) throws Exception {
		
		String result ="";
		
		//Get data dari form header
		String id_sup = request.getParameter("id_sup");
		String ket = request.getParameter("ket");
		String tgl_rtr = request.getParameter("tgl_rtr");
		
		//Set data dari form header ke model header
		ReturnHdrModel rhm = new ReturnHdrModel();
			rhm.setId_sp(Integer.valueOf(id_sup));
			SupplierModel supplier = new SupplierModel();
			supplier = this.supService.getById(Integer.valueOf(id_sup));
			rhm.setSup(supplier);
			rhm.setKet(ket);
			rhm.setTgl_rtr(tgl_rtr);
		
		
		try {
			//simpan pembelian header
			this.hdrService.insert(rhm);
			int headerID = rhm.getId();
			
			String jumlahDetail = request.getParameter("jumlahDetail");
			int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
			
			
			for (int i = 0; i < jumlahDetailInteger; i++) {
				//Set per detil satu satu
				String idBahan = request.getParameter("idBahan_"+i);
				String satuan = request.getParameter("satuan_"+i);
				String qtyDetail = request.getParameter("qty_"+i);
				String namaBhn = request.getParameter("namaBhn_"+i);
				
				BahanModel item = new BahanModel();
				item = this.bhnService.getById(Integer.valueOf(idBahan));
				
				
				ReturnDtlModel bdm = new ReturnDtlModel();
					bdm.setId_bhn(Integer.valueOf(idBahan));
					// rubah sementara
					bdm.setBahan(item);
					bdm.setSatuan(satuan);
					bdm.setId_rtr(headerID);
					bdm.setNama(namaBhn);
					bdm.setQty(Integer.valueOf(qtyDetail));
				
				//Simpan per detil satu satu
				this.dtlService.insert(bdm);
				
				int qty_now = item.getStok();
				int qty_input = Integer.valueOf(qtyDetail);
				int qty_baru = qty_now - qty_input;
				item.setStok(qty_baru);
				
				this.bhnService.update(item);
			}
			
			result="berhasil";
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			result ="gagal";
			
		}
		
		model.addAttribute("result",result);
		
		return "return";
	}

}	

