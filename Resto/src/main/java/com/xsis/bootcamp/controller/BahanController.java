package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.BahanModel;
import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.BahanService;
import com.xsis.bootcamp.service.SupplierService;

@Controller
public class BahanController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BahanService service;
	
	@Autowired
	private SupplierService supService;
	
	@RequestMapping(value="/bahan")
	public String index(Model model){
		
		return "bahan";
	}
	
	@RequestMapping(value="/bahan/list")
	public String list(Model model){
		// membuat object list dari class bahan model
		List<BahanModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "bahan/list";
	}
	
	@RequestMapping(value="/bahan/add")
	public String add(Model model){
		List<SupplierModel> supList = null;
		
		try {
			supList = this.supService.get();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		model.addAttribute("supList", supList);
		return "bahan/add";
	}
	
	@RequestMapping(value="/bahan/save")
	public String save(Model model, @ModelAttribute BahanModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "bahan/save";
	}
	
	@RequestMapping(value="/bahan/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Item model
		BahanModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "bahan/edit";
	}
	
	@RequestMapping(value="/bahan/detail")
	public String detail(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Item model
		BahanModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "bahan/detail";
	}
	
	@RequestMapping(value="/bahan/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Item model
		BahanModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "bahan/delete";
	}
}
