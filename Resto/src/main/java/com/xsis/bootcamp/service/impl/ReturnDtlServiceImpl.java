package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.ReturnDtlDao;
import com.xsis.bootcamp.model.ReturnDtlModel;
import com.xsis.bootcamp.service.ReturnDtlService;

@Service
@Transactional
public class ReturnDtlServiceImpl implements ReturnDtlService {
	
	@Autowired
	private ReturnDtlDao dao;

	@Override
	public List<ReturnDtlModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(ReturnDtlModel model) throws Exception {
		this.dao.insert(model);
		
	}

	@Override
	public ReturnDtlModel getById(String id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(ReturnDtlModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(ReturnDtlModel model) throws Exception {
		this.dao.delete(model);
	}

}
