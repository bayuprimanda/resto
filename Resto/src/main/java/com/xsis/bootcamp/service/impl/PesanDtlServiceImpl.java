package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PesanDtlDao;
import com.xsis.bootcamp.model.PesanDtlModel;
import com.xsis.bootcamp.service.PesanDtlService;

@Service
@Transactional
public class PesanDtlServiceImpl implements PesanDtlService {
	
	@Autowired
	private PesanDtlDao dao;

	@Override
	public List<PesanDtlModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(PesanDtlModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public PesanDtlModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(PesanDtlModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(PesanDtlModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
