package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PembelianDtlDao;
import com.xsis.bootcamp.model.DetailPembelianModel;
import com.xsis.bootcamp.service.PembelianDtlService;

@Service
@Transactional
public class PembelianDtlServiceImpl implements PembelianDtlService{

	@Autowired
	private PembelianDtlDao dao;
	
	@Override
	public List<DetailPembelianModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(DetailPembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public DetailPembelianModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(DetailPembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(DetailPembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
