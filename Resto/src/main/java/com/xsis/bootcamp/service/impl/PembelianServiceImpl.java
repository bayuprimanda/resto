package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.service.PembelianService;

@Service
@Transactional
public class PembelianServiceImpl implements PembelianService{

	@Autowired
	private PembelianDao dao;
	
	@Override
	public List<PembelianModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(PembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public PembelianModel getById(String id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(PembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(PembelianModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
