package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PembelianModel;

public interface PembelianService {
	
	public List<PembelianModel> get() throws Exception;

	public void insert(PembelianModel model) throws Exception;

	public PembelianModel getById(String id) throws Exception;

	public void update(PembelianModel model) throws Exception;

	public void delete(PembelianModel model) throws Exception;

}
