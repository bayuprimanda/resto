package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.ReturnHdrDao;
import com.xsis.bootcamp.model.ReturnHdrModel;
import com.xsis.bootcamp.service.ReturnHdrService;

@Service
@Transactional
public class ReturnHdrServiceImpl implements ReturnHdrService {
	
	@Autowired
	private ReturnHdrDao dao;

	@Override
	public List<ReturnHdrModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(ReturnHdrModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public ReturnHdrModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(ReturnHdrModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(ReturnHdrModel model) throws Exception {
		this.dao.delete(model);
	}

}
