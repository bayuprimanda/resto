package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PesanHdrDao;
import com.xsis.bootcamp.model.PesanHdrModel;
import com.xsis.bootcamp.service.PesanHdrService;

@Service
@Transactional
public class PesanHdrServiceImpl implements PesanHdrService{
	
	@Autowired
	private PesanHdrDao dao;
	

	@Override
	public List<PesanHdrModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(PesanHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public PesanHdrModel getById(String id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(PesanHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(PesanHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	

}
