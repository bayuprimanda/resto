package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.DetailPembelianModel;

public interface PembelianDtlService {
	
	public List<DetailPembelianModel> get() throws Exception;

	public void insert(DetailPembelianModel model) throws Exception;

	public DetailPembelianModel getById(int id) throws Exception;

	public void update(DetailPembelianModel model) throws Exception;

	public void delete(DetailPembelianModel model) throws Exception;

}
