package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.SupplierService;
import com.xsis.bootcamp.dao.SupplierDao;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService{
	
	@Autowired
	private SupplierDao dao;

	@Override
	public List<SupplierModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public SupplierModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}
	
	
	@Override
	public void insert(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	

	@Override
	public void update(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public List<SupplierModel> getByIdNotIn(String idExcludeList) {
		// TODO Auto-generated method stub
		return null;
	}

}
