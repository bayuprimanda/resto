package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.ReturnDtlModel;

public interface ReturnDtlService {

	public List<ReturnDtlModel> get() throws Exception;

	public void insert(ReturnDtlModel model) throws Exception;

	public ReturnDtlModel getById(String id) throws Exception;

	public void update(ReturnDtlModel model) throws Exception;

	public void delete(ReturnDtlModel model) throws Exception;
}
