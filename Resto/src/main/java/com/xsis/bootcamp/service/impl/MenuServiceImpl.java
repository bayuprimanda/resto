package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.model.MenuModel;
import com.xsis.bootcamp.service.MenuService;
import com.xsis.bootcamp.dao.MenuDao;

@Service
@Transactional
public class MenuServiceImpl implements MenuService{
	
	@Autowired
	private MenuDao dao;

	@Override
	public List<MenuModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}
	
	@Override
	public MenuModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void insert(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}


	@Override
	public void update(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public List<MenuModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getByIdNotIn(idExcludeList);
	}

}
