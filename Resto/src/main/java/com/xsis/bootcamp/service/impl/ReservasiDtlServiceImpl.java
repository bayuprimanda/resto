package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.ReservasiDtlDao;
import com.xsis.bootcamp.model.DetailReservasiModel;
import com.xsis.bootcamp.service.ReservasiDtlService;

@Service
@Transactional
public class ReservasiDtlServiceImpl implements ReservasiDtlService{

	@Autowired
	private ReservasiDtlDao dao;
	
	@Override
	public List<DetailReservasiModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(DetailReservasiModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public DetailReservasiModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(DetailReservasiModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(DetailReservasiModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
