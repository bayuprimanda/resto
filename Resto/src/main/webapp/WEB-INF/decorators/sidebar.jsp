<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-server"></i> <span>Data Master</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/supplier.html" class="menu-item"><i class="fa fa-circle-o"></i> Supplier</a></li>
					<li><a href="${contextName}/menu.html" class="menu-item"><i class="fa fa-circle-o"></i> Menu</a></li>
					<li><a href="${contextName}/bahan.html" class="menu-item"><i class="fa fa-circle-o"></i> Bahan Baku</a></li>
					<li><a href="${contextName}/meja.html" class="menu-item"><i class="fa fa-circle-o"></i> Meja</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Logistik</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/pembelian.html"><i class="fa fa-circle-o"></i> Pembelian Bahan</a></li>
					<li><a href="${contextName}/return.html"><i class="fa fa-circle-o"></i> Pengembalian Bahan</a></li>
					
					
				</ul>
			</li>
			
			<li>
				<a href="#"> 
					<i	class="fa fa-calculator"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/pesan.html"><i class="fa fa-circle-o"></i> Pemesanan</a></li>
					<li><a href="${contextName}/reservasi.html"><i class="fa fa-circle-o"></i> Reservasi</a></li>
				</ul>
			</li>
			
			<li>
				<a href="#"> 
					<i	class="fa fa-list-alt"></i> <span>List</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/listbeli.html"><i class="fa fa-circle-o"></i>List Pembelian Bahan</a></li>
					<li><a href="${contextName}/listreturn.html"><i class="fa fa-circle-o"></i>List Pengembalian Bahan</a></li>
					<li><a href="${contextName}/listrsv.html"><i class="fa fa-circle-o"></i> List Reservasi</a></li>
					<li><a href="${contextName}/listpesan.html"><i class="fa fa-circle-o"></i>List Pemesanan Makanan</a></li>
				</ul>
			</li>
			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>