<form id="form-menu" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id_mkn" name="id_mkn" class="form-control" value="${item.id_mkn}">
		<input type="hidden" id="n_mkn" name="n_mkn" class="form-control" value="${item.n_mkn}">
		<input type="hidden" id="jns_mkn" name="jns_mkn" class="form-control" value="${item.jns_mkn}">
		<input type="hidden" id="hrg_mkn" name="hrg_mkn" class="form-control" value="${item.hrg_mkn}">
		<input type="hidden" id="stock" name="stock" class="form-control" value="${item.stock}">
		
		<div class="form-group">
			<p align="center">Apakah anda yakin mau menghapus data menu ${item.n_mkn} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
