<form id="form-menu" method="post">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-2">ID Menu</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control" value="${list.id_mkn}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Menu</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${list.n_mkn}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<input type="text" id="jenis" name="jenis" class="form-control" value="${list.jns_mkn}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="text" id="stock" name="stock" class="form-control" value="${list.stock}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga Menu</label>
			<div class="col-md-6">
				<input type="text" id="harga" name="harga" class="form-control" value="${list.hrg_mkn}" readonly="readonly">
			</div>					
		</div>
		
	</div>
	
</form>