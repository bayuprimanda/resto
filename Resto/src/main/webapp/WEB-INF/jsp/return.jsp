<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%
	Date date = new Date();
%>


<form id="form-return-cetak" method="post">
	<section class="forms">
		<div class="box box-info">
			<div class="box-header">
				<header>
					<h1 class="h3 display">FORM PENGEMBALIAN</h1>
				</header>
			</div>
			<div class="box-body">	
				<div class="form-group">
					<label class="control-label col-md-4">Tgl Pengembalian</label>
					<div class="col-md-6">
						<input type="date" id="tgl_rtr" name="tgl_rtr" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4">Supplier</label>
					<div class="col-md-6">
						<select id="id_sup" name="id_sup" class="form-control">
							<c:forEach var="supplier" items="${listSupplier}">
								<option value="${supplier.id_sup}">${supplier.nama}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4">Ket</label>
					<div class="col-md-6">
						<input type="text" id="ket" name="ket" placeholder="Keterangan" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<div class="box-tools" style="margin-left: 18px">
						<button type="button" id="btn-add"
							class="btn btn-primary pull-left">
							<i class="fa fa-plus"></i> Tambah
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-body">
				<table class="table" id="tableDetail">
					<thead>
						<tr>
							<th>Bahan</th>
							<th>Satuan</th>
							<th>Qty</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>				
			</div>
		</div>
		<button type="submit" 
			    class="btn btn-primary pull-left">
			<i  class="fa fa-print"></i> Simpan
		</button>	
	</section>
</form>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>FORM PENGEMBALIAN</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<script>

//FUNGSI DELETE
function del(id) {
	var idx = id;
	$.ajax({
		url:'return/batal_return.json',
		type:'post',
		data: { idx : idx},
		
		success:function(data){
		
			if(data.success){
				
				$('#tableDetail').empty();
				$('#tableDetail').append(' '
						+ ' <tr> '
						+ ' <th></th> '
						+ ' <th></th> '
						+ '   <th>Bahan</th> '
						+ '   <th>Satuan</th> '
						+ '   <th>Jumlah</th> '
						+ '   <th>Action</th> '
						+ ' </tr> ');
				$.each(data.rtrList, function(index,jdt){
					$('#tableDetail').append(' ' 						
							+ ' <tr> '
							+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.rtrList.length+'"/></td> '
							+ '  <td><input type="hidden"  id="idBahan_'+index+'" name="idBahan_'+index+'" value="'+jdt.bahan.id+'"/></td> '
							+ '  <td><input type="hidden"  id="namaBhn_'+index+'" name="namaBhn_'+index+'" value="'+jdt.bahan.nama+'"/>'+jdt.bahan.nama+'</td> '
						    + '  <td><input type="hidden"  id="satuan_'+index+'"  name="satuan_'+index+'"  value="'+jdt.bahan.satuan+'"/>' + jdt.bahan.satuan + '</td> '
						    + '  <td><input type="number" min="0" max="'+jdt.bahan.stok+'"   id="qty_'+index+'" name="qty_'+index+'" style="width: 30px;" required="required"/></td> '
							+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="del('+jdt.bahan.id+');"><i class="fa fa-trash"></i></button></td> '
						    + ' </tr> ');
				});
				
			}
		}
	});
	
}
$(document).ready(function() {
	
	
	// POP-UP Btn-add
	$("#btn-add").on("click",
			function() {
				$.ajax({
					url : 'return/add.html',
					type : 'get',
					dataType : 'html',
					success : function(data) {
						$("#modal-input").find(".modal-body").html(data);
						$("#modal-input").modal('show');
					}
				});

			});
	
	// FUNGSI SAVE
	$("#form-return-cetak").submit(function() {
		$.ajax({
			url : 'return/simpan.json',
			type : 'post',
			data : $(this).serialize(), //SERIALIZE : UNTUK MENGAMBIL SELURUH NILAI YG DIINPUT PADA FORM
			dataType : 'json',
			success : function(data) {
				if(data.result=="berhasil"){
				alert("data berhasil disimpan");
			}
			else{
				alert("Data Gagal Disimpan");
				}
			}
		});
	});

	// button save ke list sementara
	$("#modal-input").on("submit","#form-return",function() {		
		var rtrPilih = new Array();			
		$('input[name="rtrPilih"]:checked').each(			
			function() {rtrPilih.push(this.value);					
		});
						
						var itemPilih = JSON.stringify(rtrPilih);
						console.log(itemPilih);
						$
								.ajax({
									url : 'return/pilih_return.json',
									type : 'post',
									data : { itemPilih : itemPilih},
									dataType : 'json',
									success : function(data) {

										if (data.success) {
											$("#modal-input").modal('hide');
											$('#tableDetail').empty();
											$('#tableDetail').append(
															' '
													+ ' <tr> '
													+ ' <th></th> '
													+ ' <th></th> '
													+ '   <th>Bahan</th> '
													+ '   <th>Satuan</th> '
													+ '   <th>Jumlah</th> '
													+ '   <th>Action</th> '
													+ ' </tr> ');
											
											
									$.each(data.rtrList,function(index,jdt) {
											$('#tableDetail').append(' '								
													+ ' <tr> '
													+ '  <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.rtrList.length+'"/></td> '
													+ '  <td><input type="hidden"  id="idBahan_'+index+'" name="idBahan_'+index+'" value="'+jdt.bahan.id+'"/></td> '
													+ '  <td><input type="hidden"  id="namaBhn_'+index+'" name="namaBhn_'+index+'" value="'+jdt.bahan.nama+'"/>'+jdt.bahan.nama+'</td> '
												    + '  <td><input type="hidden"  id="satuan_'+index+'"  name="satuan_'+index+'"  value="'+jdt.bahan.satuan+'"/>' + jdt.bahan.satuan + '</td> '
												    + '  <td><input type="number" min="0" max="'+jdt.bahan.stok+'"  id="qty_'+index+'" name="qty_'+index+'" style="width: 30px;" required="required"/></td> '
													+ '   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="del('+jdt.bahan.id+');"><i class="fa fa-trash"></i></button></td> '
												    + ' </tr> ');
															});
										}
									}
								});
						return false;
					});
});
</script>