<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Pemesanan</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pemesanan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						No.
				</td>
					<td>
						${item.id}
					</td>
				</tr>
				 <tr>
                <td>
						Tgl. Pengembalian
				</td>
					<td>
						${item.tgl_rtr}
					</td>
				</tr>
				 <tr>
                <td>
						Keterangan
				</td>
					<td>
						${item.ket}
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
						<th>Id Bahan</th>
						<th>Nama</th>
						<th>Satuan</th>
						<th>Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemDetail" items="${item.detail}">
					<tr>
						<td>${itemDetail.id_bhn}</td>
						<td>${itemDetail.nama}</td>
						<td>${itemDetail.satuan}</td>
						<td>${itemDetail.qty}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="listreturn.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>