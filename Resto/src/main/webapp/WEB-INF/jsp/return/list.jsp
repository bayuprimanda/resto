<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${listReturn}">
	<tr>
		<td>${item.id}</td>
		<td>${item.tgl_rtr}</td>
		<td>${item.ket}</td>
		<td>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${item.id}"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>