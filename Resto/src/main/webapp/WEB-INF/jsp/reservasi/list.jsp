<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${listReservasi}">
	<tr>
		<td>${item.nama}</td>
		<td>${item.tgl}</td>
		<td>${item.jam}</td>
		<td>${item.hp}</td>
		<td>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${item.id }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>