<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Pemesanan</h3>
	</div>
	<div class="box-body">
		<table class="table" id="pesan">
			<thead>
				<tr>
					<th>No. Meja</th>
					<th>Tanggal</th>
					<th>Tipe Bayar</th>
					<th>Total Harga</th>
					<th>Bayar</th>
					<th>Kembalian</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Detail Pemesanan</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<script>

	  $(function () {
	  $("#pesan").DataTable();
		});

	// list data meja
	function loadData(){
		$.ajax({
			url:'pesan/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	loadData();
	
	$(document).ready(function(){
		
		
		// button detail
		$("#list-data").on("click",".btn-detail",function(){
			var vId = $(this).val();
			$.ajax({
				url:'pesan/detail.html',
				type:'get',
				data:{ id:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
	});
</script>
