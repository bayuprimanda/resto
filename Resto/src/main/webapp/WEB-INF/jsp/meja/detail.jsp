<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" >
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Meja</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${item.nomor}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<input type="text" id="jenis" name="jenis" class="form-control" value="${item.tipe}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="text" id="status" name="status" class="form-control" value="${item.status}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="text" id="ket" name="ket" class="form-control" value="${item.ket}" readonly="readonly">
			</div>					
		</div>
		
	</div>

</form>
