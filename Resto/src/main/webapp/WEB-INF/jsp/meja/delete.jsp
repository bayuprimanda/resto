<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Meja</label>
			<div class="col-md-6">
				<input type="hidden" id="nama" name="nama" class="form-control" value="${item.nomor}">
				<input type="text" id="dnama" name="dnama" class="form-control" value="${item.nomor}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<input type="hidden" id="jenis" name="jenis" class="form-control" value="${item.tipe}">
				<input type="text" id="djenis" name="djenis" class="form-control" value="${item.tipe}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="hidden" id="stok" name="stok" class="form-control" value="${item.status}">
				<input type="text" id="dstok" name="dstok" class="form-control" value="${item.status}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="hidden" id="stok" name="stok" class="form-control" value="${item.ket}">
				<input type="text" id="dstok" name="dstok" class="form-control" value="${item.ket}" disabled="disabled">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="col-md-6">Apakah anda yakin mau menghapus data meja no. ${item.nomor} tipe ${item.tipe} ?</label>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Hapus</button>
	</div>
</form>
