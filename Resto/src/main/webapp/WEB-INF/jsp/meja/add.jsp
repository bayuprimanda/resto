<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">No.</label>
			<div class="col-md-6">
				<input type="text" pattern="[A-Z0-9]+" id="nomor" name="nomor" placeholder="Nomor Meja (ex :12A)" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe" required>
					<option value="">---Pilih Tipe---</option>
					<option value="Family">Family</option>
					<option value="Smoking-Date">Smoking-Date</option>
					<option value="Non-Smoking-Date">Non-Smoking-Date</option>
					<option value="Non-smoking-Family">Non-smoking-Family</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="radio" name="status" value="Tersedia" required>Tersedia <br>
				<input type="radio" name="status" value="Tidak-tersedia" required>Tidak-tersedia
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Ket</label>
			<div class="col-md-6">
				<input type="text" id="ket" name="ket" placeholder="Keterangan" class="form-control" required="required">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
