<form id="form-bahan" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id}">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Bahan</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${item.nama}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<input type="text" id="jenis" name="jenis" class="form-control" value="${item.jenis}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="text" id="stok" name="stok" class="form-control" value="${item.stok}" readonly="readonly">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<input type="text" id="satuan" name="satuan" class="form-control" value="${item.satuan}" readonly="readonly">
			</div>					
		</div>
		
	</div>
	
</form>
