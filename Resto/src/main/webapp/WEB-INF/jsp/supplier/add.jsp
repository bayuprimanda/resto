<form id="form-supplier" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">NAMA</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" placeholder="Nama Supplier" class="form-control" onblur="cekno();" required="required">
				<img id="img2" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt2" style="display: none;"><font color="red">Field nama harus diisi</font></a>
			</div>					
		</div>
			
		<div class="form-group">
			<label class="control-label col-md-2">TIPE</label>
			<div class="col-md-6">
				<select id="tipe" name="tipe" required="required">
					<option value="">--- Pilih Tipe ---</option>
					<option value="PT">PT</option>
					<option value="CV">CV</option>
					<option value="Individu">Individu</option>
				</select>
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field tipe harus diisi</font></a>
			</div>					
		</div>
	
		<div class="form-group">
			<label class="control-label col-md-2">E-MAIL</label>
			<div class="col-md-6">
				<input type="email" id="email" name="email" placeholder="Akun e-mail Supplier" class="form-control" onblur="cekket();" required="required">
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field email harus diisi</font></a>
			</div>					
		</div>
	
		<div class="form-group">
			<label class="control-label col-md-2">TLP</label>
			<div class="col-md-6">
				<input type="tel" pattern="[0-9]{7,12}" id="telephon" placeholder="Nomor Telepon" name="telephon" class="form-control" onblur="cekket();" required="required">
				<img id="img3" alt="" src="${pageContext.request.contextPath}/alert.png" style="display: none;">
				<a id="alt3" style="display: none;"><font color="red">Field telepon harus diisi</font></a>
			</div>					
		</div>
	</div>
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>

<script type="text/javascript">

function cekno(){
		var nama = document.getElementById("n_sup");
		if (nama.value == null || nama.value == "") {
			nama.style.borderColor = "red";
			document.getElementById("img2").style = "block";
			document.getElementById("alt2").style = "block";
		}
		else {
			nama.style.borderColor = "black";
			document.getElementById("img2").style.display = "none";
			document.getElementById("alt2").style.display = "none";
		}
	}
	
	function cekket(){
		var stok = document.getElementById("tlp_sup");
		if (stok.value == null || stok.value == "") {
			stok.style.borderColor = "red";
			document.getElementById("img3").style = "block";
			document.getElementById("alt3").style = "block";
		}
		else {
			stok.style.borderColor = "black";
			document.getElementById("img3").style.display = "none";
			document.getElementById("alt3").style.display = "none";
		}
	}
	
</script>
