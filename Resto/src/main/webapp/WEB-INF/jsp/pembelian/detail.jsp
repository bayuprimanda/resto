<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Pembelian</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pembelian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						No.Pembelian
				</td>
					<td>
						${item.id}
					</td>
				</tr>
				<tr>
					<td>
						Tanggal
					</td>
					<td>
						${item.tgl}
					</td>
				</tr>
				<tr>
					<td>
						Tipe Bayar
					</td>
					<td>
						${item.tp_bayar}
					</td>
				</tr>
				<tr>
					<td>
						Total Harga
					</td>
					<td>
						Rp. ${item.total}
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
					<th>Nama Bahan</th>
						<th>Satuan</th>
						<th>QTY</th>
						<th>Harga Beli</th>
						<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemDetail" items="${item.detail}">
					<tr>
						<td>${itemDetail.nama_bhn}</td>
						<td>${itemDetail.satuan}</td>
						<td>${itemDetail.qty}</td>
						<td>Rp.${itemDetail.harga_beli}</td>
						<td>Rp.${itemDetail.subtotal_beli}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="listbeli.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>