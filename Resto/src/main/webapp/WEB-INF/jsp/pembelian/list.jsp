<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${listBeli}">
	<tr>
		<td>${item.id}</td>
		<td>${item.tgl}</td>
		<td>${item.tp_bayar}</td>
		<td>Rp.${item.total}</td>
		<td>Rp.${item.bayar}</td>
		<td>Rp.${item.kembalian}</td>
		<td>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${item.id }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>