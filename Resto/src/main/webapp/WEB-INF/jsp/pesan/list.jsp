<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${listPesan}">
	<tr>
		<td>${item.mj.nomor}</td>
		<td>${item.date}</td>
		<td>${item.tp_bayar}</td>
		<td>Rp.${item.total}</td>
		<td>Rp.${item.bayar}</td>
		<td>Rp.${item.kembalian}</td>
		<td>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${item.id }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>