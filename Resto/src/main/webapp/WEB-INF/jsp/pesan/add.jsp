<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-pesan" action="/action_page.php" method="post">
	<table class="table">
		<tr>
			<th>MENU</th>
			<th>HARGA</th>
			<th>PILIH</th>
		</tr>
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.n_mkn}</td>
				<td>Rp. ${item.hrg_mkn},-</td>
				<td><input type="checkbox" name="menuPilih" value="${item.id_mkn}" /></td>
			</tr>
		</c:forEach>
		<tr>
			<td><button class="btn btn-primary pull-left" type="submit">Tambah
					Pesanan</button></td>
		</tr>
	</table>

</form>

