<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Pemesanan</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pemesanan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						No. Pesan
				</td>
					<td>
						${item.id}
					</td>
				</tr>
				 <tr>
                <td>
						No. Meja
				</td>
					<td>
						${item.mj.nomor}
					</td>
				</tr>
				 <tr>
                <td>
						Nama Pemesan
				</td>
					<td>
						${item.nama}
					</td>
				</tr>
				<tr>
					<td>
						Tanggal
					</td>
					<td>
						${item.date}
					</td>
				</tr>
				<tr>
					<td>
						Tipe Bayar
					</td>
					<td>
						${item.tp_bayar}
					</td>
				</tr>
				<tr>
					<td>
						Total Harga
					</td>
					<td>
						Rp. ${item.total}
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
						<th>Pesanan</th>
						<th>QTY</th>
						<th>Harga</th>
						<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="itemDetail" items="${item.detail}">
					<tr>
						<td>${itemDetail.nm_mn}</td>
						<td>${itemDetail.qty}</td>
						<td>Rp.${itemDetail.hrg_jl}</td>
						<td>Rp.${itemDetail.subtot}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="listpesan.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>